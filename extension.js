/*
  Usar el tracking de Sweven
  Creado por Erick Tucto (erick.tucto@outlook.com)
*/
const { App } = imports.misc.extensionUtils.getCurrentExtension().imports.dist;

let app;

function init() {
}

function enable() {
  app = new App();
  app.enable();
}

function disable() {
  app.disable();
}
