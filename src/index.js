import context from '@imports/this'
import { main as Main } from '@imports/ui'
import Tracker from './components/tracker'
import { TrackingRepository } from './repositories/TrackingRepository'
import { TrackingViewModel } from './TrackingViewModel'
import Me from '@imports/me'
import { getJson } from './libs/fileJson'
import {login} from './request/auth'

const config = getJson(`${Me.path}/config.json`)

class App {
  constructor() {
    this.button = null
    this.viewModel = null
    this.repository = null
    this.server = null
  }
  async enable() {
    try {
      const user = await this.login()
      this.button = new Tracker({ token: user.api_token })
      this.repository = new TrackingRepository({
        token: user.api_token,
        userId: user.id,
        pusher: config.pusher
      })
      this.viewModel = new TrackingViewModel(this.repository, this.button)

      Main.panel.addToStatusArea('SwevenTracking', this.button, 2)
    } catch (e) {
      log('--- ERROR: sweven-tracking@ericktucto.com/extension.js ---')
      console.log(e)
      logError(e)
      log('--- ERROR: sweven-tracking@ericktucto.com/extension.js ---')
    }
  }
  async login() {
    const { email, password } = config.credenciales
    const { data } = await login({ email, password })
    return data
  }
  disable() {
    this.button.destroy()
  }
}

;(function main(exports) {
  exports.App = App
})(context)
