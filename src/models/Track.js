import { Time } from './../libs/time'
import * as tracking from './../request/tracking'

export class Track {
  constructor(data) {
    this.id = data.id
    this.userId = data.userId
    this.workOrder = data.workOrder
    this.createdAt = new Date(data.createdAt)
    this.start = new Date(data.start)
    this.end = data.end ? new Date(data.end) : null
    this.time = Time.fromDate(this.start, this.end)
  }

  get isActive() {
    return !this.end
  }

  play(api_token) {
    return tracking.start(this, api_token)
  }

  pause(api_token) {
    return tracking.pause(api_token)
  }

  toString() {
    return `${this.workOrder.code} ${this.time}`
  }
}
