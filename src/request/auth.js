import Me from "@imports/me"
import { post } from "./../libs/fetch"
import { getJson } from "./../libs/fileJson"

const HOST = getJson(`${Me.path}/config.json`).host
export const login = async ({ email, password }) => {
  const request = await post(
    `${HOST}/api/auth/login`,
    { email, password },
    { user_agent: 'Gnome Shell Extension - sweven-tracking@ericktucto.com' }
  )
  return JSON.parse(request.response_body.data)
}
