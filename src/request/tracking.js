import { get, post } from './../libs/fetch'
import { Track } from './../models/Track'
import { getJson } from './../libs/fileJson'
import Me from '@imports/me'

const HOST = getJson(`${Me.path}/config.json`).host
const parseData = message => {
  const data = JSON.parse(message.response_body.data).data
  return data.map(d => new Track(d))
}

const defaultHeaders = token => ({
  user_agent: 'Gnome Shell Extension - sweven-tracking@ericktucto.com',
  headers: { Authorization: `Bearer ${token}` }
})

export const getTracking = async ({ token, start = null, end = null, limit = 30 }) => {
  const data = {
    limit
  }
  if (start && end) {
    data.start = start
    data.end = end
  }
  const message = await get(`${HOST}/api/track`, data, defaultHeaders(token))
  return parseData(message)
}
export const start = (track, api_token) => {
  post(`${HOST}/api/track/start`, { workOrder: track.workOrder.id }, defaultHeaders(api_token))
}
export const pause = api_token => {
  get(`${HOST}/api/track/pause`, {}, defaultHeaders(api_token))
}
