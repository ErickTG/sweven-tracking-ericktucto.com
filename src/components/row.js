import GLib from '@imports/GLib'
import GObject from '@imports/GObject'
import St from '@imports/St'
import Gio from '@imports/Gio'
import { popupMenu as PopupMenu } from '@imports/ui'
import Me from '@imports/me'
import { getJson } from './../libs/fileJson'

const HOST = getJson(`${Me.path}/config.json`).host

export const RowMenuItem = GObject.registerClass({ GTypeName: "SwevenTrackingRow"},
  /**
   * @property {St.Label} labelTime
   */
  class RowMenuItem extends PopupMenu.PopupBaseMenuItem {
    _init(track, api_token) {
      this.track = track
      super._init({
        reactive: true,
        activate: false,
        hover: false,
        style_class: null,
        can_focus: true
      })
      this._sourceBinding = null
      this.loop = null
      this.label = new St.Button({
        label: track.workOrder.code,
        x_expand: true,
      })
      this.label.connect('clicked', () =>
        Gio.AppInfo.launch_default_for_uri(this.urlWorkOrder, null)
      )
      this.add_child(this.label)
      this.labelTime = new St.Label({
        text: track.time.toString(),
        x_expand: true,
      })
      this.add_child(this.labelTime)
      this.buffer = new St.Entry({
        text: `${track.workOrder.code} ${track.time.toString()}`
      })
      this._idClicked = null
      this.track = track
      this._button = this._createNewButton(api_token)
      this.add_child(this._button)
    }

    get isActive() {
      return this.track.isActive
    }

    get urlWorkOrder() {
      const code = this.track.workOrder.code
      return `${HOST}/work-orders/${code}`
    }

    setSourceBinding(source) {
      this._sourceBinding = source
      const { time, workOrder: { code } } = this.track
      this.buffer.set_text(`${code} ${time.toString()}`)
    }

    _createNewButton(api_token) {
      const icon_name = this.track.isActive
        ? 'media-playback-pause-symbolic'
        : 'media-playback-start-symbolic'
      const button = new St.Button()
      button.child = new St.Icon({
        style_class: 'popup-menu-icon',
        icon_name
      })
      this._idClicked = button.connect('clicked', () => {
        if (this.track.isActive) {
          this.track.pause(api_token)
        } else {
          this.track.play(api_token)
        }
      })
      return button
    }

    destroy() {
      if (this.loop) GLib.source_remove(this.loop)
      if (this._sourceBinding) this._sourceBinding.unbind()
      super.destroy()
    }
  }
)
