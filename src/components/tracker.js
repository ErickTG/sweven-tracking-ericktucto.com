import GObject from '@imports/GObject'
import St from '@imports/St'
import { panelMenu as PanelMenu } from '@imports/ui'
import { Time } from './../libs/time'
import { Track } from './../models/Track'
import { RowMenuItem } from './row'
import { Display } from './display'
import { ControlsComponent } from './controls'
import { TimeTotal } from './timetotal'

const Tracker = GObject.registerClass(
  class Tracker extends PanelMenu.Button {
    _init({ token }) {
      super._init(St.Align.START)
      // PROPIEDADES
      this.rows = []
      this.token = token
      this._displayLabel = new Display()
      this.add_child(this._displayLabel)
      this._controls = new ControlsComponent()
      this._timetotal = new TimeTotal('00:00:00')
      this.menu.addMenuItem(this._controls)
      this.menu.addMenuItem(this._timetotal)
    }

    clearRows() {
      if (this.rows.length) {
        this.rows.forEach(r => r.destroy())
      }
      this.rows = []
    }

    /**
     * @param {Track[]} tracks
     */
    addTracks(tracks) {
      tracks.forEach(track => {
        const row = new RowMenuItem(track, this.token)
        this.menu.addMenuItem(row, 1 + this.rows.length)
        this.rows.push(row)
      })
    }

    updateActivedRow(callback) {
      if (this.rows.find(row => row.track.isActive))
        callback(this.rows.find(row => row.track.isActive))
    }
  }
)

export default Tracker
