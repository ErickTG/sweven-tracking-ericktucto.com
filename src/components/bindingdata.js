import GObject from '@imports/GObject'
import St from '@imports/St'

export class ComponentNotFound {
  constructor(component) {
    this.name = "ComponentNotFound"
    this.message = `${component} no esta registrado`
  }
}

export class BindingData {
  constructor() {
    this._components = {
      display: new St.Entry({ text: "" }),
      total: new St.Entry({ text: "" }),
      activedTrack: new St.Entry({ text: "" }),
    }
  }

  binding(component, widget, property = 'text') {
    if (!this._components[component]) {
      throw new ComponentNotFound(component)
    }
    const source = this._components[component].bind_property(
      'text',
      widget,
      property,
      GObject.BindingFlags.BIDIRECTIONAL
    )
    return source
  }

  update(component, text) {
    if (!this._components[component]) {
      throw new ComponentNotFound(component)
    }
    this._components[component].set_text(text)
  }
}
