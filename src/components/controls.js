import GObject from '@imports/GObject'
import Clutter from '@imports/Clutter'
import St from '@imports/St'
import { popupMenu as PopupMenu } from '@imports/ui'

const ButtonWithIcon = icon => {
  const child = St.Icon.new()
  child.set_icon_name(icon)
  child.set_icon_size(24)
  return new St.Button({
    child
  })
}

export const ControlsComponent = GObject.registerClass(
  { GTypeName: 'SwevenTrackingControlsComponent' },
  class ControlsComponent extends PopupMenu.PopupBaseMenuItem {
    _init() {
      super._init({
        reactive: true,
        activate: false,
        hover: false,
        style_class: null,
        can_focus: true
      })
      this._observers = {
        back: [],
        today: [],
        forward: []
      }

      const box1 = new St.BoxLayout({
        x_expand: true,
        vertical: true
      })
      this.label = new St.Label({
        text: '',
        x_align: Clutter.ActorAlign.CENTER
      })
      box1.add_child(this.label)

      this.btnBack = ButtonWithIcon('gtk-go-back')
      this.btnToday = new St.Button({
        label: 'Hoy',
        x_expand: true
      })
      this.btnForward = ButtonWithIcon('gtk-go-forward')

      const box = new St.BoxLayout({
        x_expand: true
      })
      box.add_child(this.btnBack)
      box.add_child(this.btnToday)
      box.add_child(this.btnForward)

      box1.add_child(box)
      this.add_child(box1)
      this.btnBack.connect('clicked', (...data) => this.emit("back", data))
      this.btnToday.connect('clicked', (...data) => this.emit("today", data))
      this.btnForward.connect('clicked', (...data) => this.emit("forward", data))
    }

    emit(event, data = null) {
      /** @type {Array} */
      const observers = this._observers[event]
      observers.forEach(observer => observer(data))
    }

    on(event, callback) {
      if (event in this._observers) {
        this._observers[event].push(callback)
      } else {
        throw new EventNotFound(event)
      }
    }

    _onDestroy() {
      super.destroy()
    }
  }
)
