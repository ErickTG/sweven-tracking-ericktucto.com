import Clutter from '@imports/Clutter'
import GObject from '@imports/GObject'
import St from '@imports/St'
import { Time } from './../libs/time'

export const Display = GObject.registerClass(
  { GTypeName: 'SwevenTrackingDisplay' },
  class Component extends St.BoxLayout {
    _init(track = null) {
      super._init()
      this.track = track
      this._time = track ? track.time : null
      this._labelDisplay = new St.Label({
        text: this.track ? this.track.toString() : this.time.toString(),
        y_expand: true,
        y_align: Clutter.ActorAlign.CENTER
      })
      this.add_child(this._labelDisplay)
    }

    get time() {
      return this._time || new Time()
    }

    updateLabel(text = null) {
      if (text) {
        return this._labelDisplay.set_text(text)
      }
      if (this.track) {
        return this._labelDisplay.set_text(this.track.toString())
      }
      return this._labelDisplay.get_text()
    }
  }
)
