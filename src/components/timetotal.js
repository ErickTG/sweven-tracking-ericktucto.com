import GObject from '@imports/GObject'
import St from '@imports/St'
import Clutter from '@imports/Clutter'
import { popupMenu as PopupMenu } from '@imports/ui'

export const TimeTotal = GObject.registerClass(
  { GTypeName: 'SwevenTrackingTimeTotal' },
  /**
   * @property {St.Label} labelTime
   */
  class TimeTotal extends PopupMenu.PopupBaseMenuItem {
    _init(total) {
      super._init({
        reactive: true,
        activate: false,
        hover: false,
        style_class: null,
        can_focus: true
      })
      this.label = new St.Label({
        text: total,
        x_expand: true,
        x_align: Clutter.ActorAlign.CENTER,
      })
      this.add_child(this.label)
    }
  }
)
