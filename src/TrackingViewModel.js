import GLib from '@imports/GLib'
import { Track } from './models/Track'
import { BindingData } from './components/bindingdata'
import { Time } from './libs/time'

const DAYS = {
  0: 'Domingo',
  1: 'Lunes',
  2: 'Martes',
  3: 'Miercoles',
  4: 'Jueves',
  5: 'Viernes',
  6: 'Sabado'
}

const MONTHS = {
  0: 'Enero',
  1: 'Febrero',
  2: 'Marzo',
  3: 'Abril',
  4: 'Mayo',
  5: 'Junio',
  6: 'Julio',
  7: 'Agosto',
  8: 'Setiembre',
  9: 'Octubre',
  10: 'Noviembre',
  11: 'Diciembre'
}

/**
 * @param {Date} day
 */
const stringDate = day => `${day.getFullYear()}${day.getMonth()}${day.getDate()}`

/**
 * @param {Track[]} tracks
 * @returns {Time}
 */
const sumTracks = tracks => tracks.reduce((acc, track) => acc.sum(track.time), new Time())

/**
 * @property {TrackingRepository} repository
 * @property {BindingData} _bindingdata
 */
export class TrackingViewModel {
  constructor(repository, trackerComponent) {
    this.repository = repository
    this.trackerComponent = trackerComponent
    this._day = new Date()
    this._bindingdata = new BindingData()
    this._tracks = {}
    this._loop = null
    /** @type {Track|null} */
    this._activedTrack = null
    this.startup()
    this.registerEvents()
  }

  async startup() {
    try {
      this.bindingComponents()
      await this.fetchActive()
      await this.changeDay(new Date())
    } catch (e) {
      log('---------------')
      logError(e)
      log('---------------')
    }
  }

  bindingComponents() {
    this._bindingdata.binding('total', this.trackerComponent._timetotal.label)
    this._bindingdata.binding('display', this.trackerComponent._displayLabel._labelDisplay)
  }

  async fetchActive() {
    const track = await this.repository.fetchActive()
    if (track) this._activedTrack = track
  }

  async fetch() {
    if (this.stringDate in this._tracks) {
      return [...this._tracks[this.stringDate]]
    }
    /** @type {Track[]} */
    const tracks = await this.repository.fetch(this._day)
    this._tracks[stringDate(this._day)] = tracks
  }

  registerEvents() {
    this.repository.on('tracker:started', this.on_started_tracker.bind(this))
    this.repository.on('tracker:paused', this.on_paused_tracker.bind(this))
    this.trackerComponent._controls.on('back', this.on_clicked_back.bind(this))
    this.trackerComponent._controls.on('today', this.on_clicked_today.bind(this))
    this.trackerComponent._controls.on('forward', this.on_clicked_forward.bind(this))
  }

  on_started_tracker(data) {
    const { started } = data
    const track = new Track(started)
    const key = stringDate(track.start)
    this._activedTrack = track
    if (key in this._tracks) {
      this._tracks[key].unshift(track)
    } else {
      this._tracks[key] = [track]
    }
    this.reloadListTracks()
  }

  on_paused_tracker({ paused = null }) {
    if (paused) {
      this._activedTrack = null
      const key = stringDate(new Date(paused.start))
      if (!this._tracks[key]) {
        this._tracks[key] = []
      }
      const index = this._tracks[key].findIndex(t => t.id == paused.id)
      if (index >= 0) {
        this._tracks[key][index].end = paused.end
      } else {
        this._tracks[key] = [new Track(paused)]
      }
      this.reloadListTracks()
    }
  }

  async on_clicked_back() {
    const newDay = new Date(this._day)
    newDay.setDate(this._day.getDate() - 1)
    await this.changeDay(newDay)
  }

  async on_clicked_today() {
    await this.changeDay(new Date())
  }

  async on_clicked_forward() {
    const newDay = new Date(this._day)
    newDay.setDate(this._day.getDate() + 1)
    await this.changeDay(newDay)
  }

  /**
   * @param {Date} newDay
   */
  async changeDay(newDay) {
    this._day = newDay
    this.updateLabelDate()
    await this.fetch()
    this.reloadListTracks()
  }

  updateLabelDate() {
    this.trackerComponent._controls.label.set_text(this.textDay)
  }

  reloadListTracks() {
    this.trackerComponent.clearRows()
    this.trackerComponent.addTracks(this.tracks)

    this._bindingdata.update('total', this.total.toString())

    if (this._activedTrack) {
      this.trackerComponent.updateActivedRow(row => {
        const source = this._bindingdata.binding('activedTrack', row.labelTime)
        row.setSourceBinding(source)
      })
      if (!this._loop) this.loopUpdateTime()
    } else {
      this._bindingdata.update('display', sumTracks(this.today).toString())
    }
  }

  loopUpdateTime() {
    this._loop = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, 1, () => {
      try {
        if (!this._activedTrack) {
          this._loop = null
          return GLib.SOURCE_REMOVE
        }
        const key = stringDate(this._activedTrack.start)
        const index = this._tracks[key].findIndex(t => t.id == this._activedTrack.id)
        this._tracks[key][index].time = this._tracks[key][index].time.sum(new Time('00:00:01'))

        const track = this._tracks[key][index]
        this._bindingdata.update('activedTrack', track.time.toString())
        this._bindingdata.update('display', track.toString())
        this._bindingdata.update('total', this.total.toString())
        return GLib.SOURCE_CONTINUE
      } catch (e) {
        console.log('--------------')
        console.log(e)
        console.log('--------------')
        return GLib.SOURCE_REMOVE
      }
    })
  }

  /**
   * @returns {String}
   */
  get stringDate() {
    return stringDate(this._day)
  }

  /**
   * @returns {Track[]}
   */
  get today() {
    const nameDate = stringDate(new Date())
    return nameDate in this._tracks ? [...this._tracks[nameDate]] : []
  }

  /**
   * @returns {Time}
   */
  get total() {
    return sumTracks(this.tracks)
  }

  /**
   * @returns {Track[]}
   */
  get tracks() {
    return this.stringDate in this._tracks ? [...this._tracks[this.stringDate]] : []
  }

  get textDay() {
    const day = DAYS[this._day.getDay()]
    const month = MONTHS[this._day.getMonth()]
    return `${day} ${this._day.getDate()} de ${month}`
  }
}
