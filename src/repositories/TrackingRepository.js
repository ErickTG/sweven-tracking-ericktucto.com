import { Pusher } from '../libs/pusher/pusher'
import { getTracking } from '../request/tracking'

export class EventNotFound {
  constructor(event) {
    this.name = 'EventNotFound'
    this.message = `No puedes registrar al evento: ${event}, este no existe`
  }
}

export class TrackingRepository {
  constructor({ pusher, token, userId }) {
    this.token = token
    this._client = new Pusher(pusher)
    this._observers = {
      'tracker:started': [],
      'tracker:paused': []
    }
    this._client
      .subscribe(`tracker.${userId}`)
      .listen('started', data => {
        this.emit('tracker:started', data)
      })
      .listen('paused', data => {
        this.emit('tracker:paused', data)
      })
  }

  emit(event, data = null) {
    /** @type {Array} */
    const observers = this._observers[event]
    observers.forEach(observer => observer(data))
  }

  on(event, callback) {
    if (event in this._observers) {
      this._observers[event].push(callback)
    } else {
      throw new EventNotFound(event)
    }
  }

  /**
   * @param {Date} date
   */
  async fetch(date) {
    const start = new Date()
    const end = new Date()
    start.setDate(date.getDate() - 1)
    end.setDate(date.getDate() + 1)
    const tracks = await getTracking({
      token: this.token,
      start: this._formatter(start) + ' 23:59:00',
      end: this._formatter(end) + ' 00:00:00'
    })
    return tracks
  }

  async fetchActive() {
    const tracks = await getTracking({
      token: this.token,
      limit: 1
    })
    if (tracks.length && tracks[0].isActive) {
      return tracks[0]
    }
    return null
  }

  /**
   * @param {Date} date
   */
  _formatter(date) {
    const withZero = n => (n < 10 ? `0${n}` : n)
    const month = withZero(date.getMonth() + 1)
    const day = withZero(date.getDate())
    const year = date.getFullYear()
    return `${year}-${month}-${day}`
  }
}
