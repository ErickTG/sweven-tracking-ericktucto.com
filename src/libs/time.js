export class TimeFromStringException {
  constructor() {
    this.name = 'TimeFromStringException'
    this.message = 'El formato es incorrecto. Se debe usar hh:mm:ss'
  }
}
export class TimeInvalidArgument {
  constructor() {
    this.name = 'TimeInvalidArgument'
    this.message = 'El argumento es valido debe ser de tipo Date o null'
  }
}
export class Time {
  constructor(timeString = '00:00:00') {
    if (!timeString) {
      this.hours = 0
      this.minutes = 0
      this.seconds = 0
    } else if (typeof timeString === 'string') {
      const { hours, minutes, seconds } = this.parseTime(timeString)
      this.hours = hours
      this.minutes = minutes
      this.seconds = seconds
    } else {
      throw new TimeFromStringException()
    }
  }

  /**
   * @param {string} timeString
   */
  parseTime(timeString) {
    const reg = /^(\d*):([0-5][0-9]):([0-5][0-9])$/
    const matched = timeString.match(reg)
    if (matched) {
      const [_, hours, minutes, seconds] = matched
      return { hours: +hours, minutes: +minutes, seconds: +seconds }
    } else {
      console.log(timeString)
      throw new TimeFromStringException()
    }
  }

  /**
   * @param {?Date} start
   * @param {?Date} end
   */
  static fromDate(start = null, end = null) {
    end = !end ? new Date() : end
    if (start instanceof Date && end instanceof Date) {
      const diferencia = Math.abs(start - end) // DIFERENCIA EN MILLISEGUNDOS
      let seconds = Math.trunc(diferencia / 1000)
      const hours = Math.trunc(seconds / (60 * 60))
      seconds -= hours * 60 * 60
      const minutes = Math.trunc(seconds / 60)
      seconds -= minutes * 60
      const parsed = Time.parse(hours, minutes, seconds)
      return new Time(parsed)
    }
    throw new TimeInvalidArgument()
  }

  /**
   * @param {string} timeString
   * @return {typeof this}
   */
  static create(timeString) {
    return new this(timeString)
  }

  /**
   * @param {typeof this} time
   * @return {typeof this}
   */
  sum(time) {
    /**
     * @param {Number} num1
     * @param {Number} num2
     * @return {Array}
     */
    const intAndRest = (num1, num2) => [(num1 + num2) % 60, Math.floor((num1 + num2) / 60)]
    const [seconds, restSeconds] = intAndRest(this.seconds, time.seconds)
    const [minutes, restMinutes] = intAndRest(this.minutes, time.minutes + restSeconds)
    const hours = this.hours + time.hours + restMinutes
    const timeString = Time.parse(hours, minutes, seconds)
    return Time.create(timeString)
  }

  static parse(hours = 0, minutes = 0, seconds = 0) {
    hours = hours > 9 ? hours : `0${hours}`
    minutes = minutes > 9 ? minutes : `0${minutes}`
    seconds = seconds > 9 ? seconds : `0${seconds}`
    return [hours, minutes, seconds].join(':')
  }

  toString() {
    return Time.parse(this.hours, this.minutes, this.seconds)
  }
}
