import Soup from '@imports/Soup';
const byteArray = imports.byteArray;


export const byteArrayToString = (data) =>
  byteArray.toString(byteArray.fromGBytes(data));

export const dataToStringOrJson = (data) => {
  if (!typeof data === "string") {
    return data;
  }
  try {
    const dataJson = JSON.parse(data);
    return dataJson;
  } catch (e) {
    return data;
  }
};

export const getLibSoup = async (version) => {
  try {
    return Soup;
  } catch (e) {
    logError("--- @ericktucto/pusher:src/common.js ---");
    logError(e);
    logError("--- @ericktucto/pusher:src/common.js ---");
    return null;
  }
}

