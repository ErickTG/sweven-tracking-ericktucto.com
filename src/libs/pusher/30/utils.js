import GLib from "@imports/GLib";
import { getLibSoup } from "./../common.js";

export const createConnection = async (uri, protocol = "ws") =>
  new Promise(async (resolve, reject) => {
    const Soup = await getLibSoup("3.0");
    if (!Soup) resolve(null);
    const session = new Soup.Session();
    uri = GLib.Uri.parse(uri, GLib.UriFlags.NONE);
    const message = new Soup.Message({ method: "GET", uri });
    session.websocket_connect_async(
      message,
      "origin",
      [protocol],
      null,
      null,
      (_session, res) => {
        try {
          resolve(session.websocket_connect_finish(res));
        } catch (e) {
          reject(e);
        }
      }
    );
  });

