import { printError, printMessage, createConnection as createConnection2 } from './24/utils.js'
import { byteArrayToString, dataToStringOrJson } from './common.js'
import { createConnection as createConnection3 } from './30/utils.js'

const TEXT = 1

export class Channel {
  constructor(name, pending = false) {
    this.name = name
    this.pending = pending
    this.events = {}
  }
  add(event, callback) {
    if (event in this.events) {
      this.events[event].push(callback)
    } else {
      this.events[event] = [callback]
    }
  }
  dispatch(event, data) {
    if (event in this.events) {
      this.events[event].forEach(callback => callback(data))
    }
  }
}

export class Pusher {
  constructor({ key, cluster }) {
    this.connection = null
    this.channels = {}
    this.pendingChannels = []
    this.selectedChannel = null
    const uri = `ws://ws-${cluster}.pusher.com:80/app/${key}?client=gjs&version=1.0&protocol=5`
    this._createConnection({ uri })
  }

  async _createConnection({ uri, autoClose = false, versionSoup = 2 }) {
    try {
      this.connection =
        versionSoup == 2 ? await createConnection2(uri) : await createConnection3(uri)
      this.connection.connect('closed', this._closedConnection.bind(this))
      this.connection.connect('error', this._errorConnection.bind(this))
      this.connection.connect('message', this._messageConnection.bind(this))
      this._proccessPendingChannels()
    } catch (e) {
      printError('--- @ericktucto/pusher:src/pusher.js@_createConnection ---', e)
      if (autoClose) {
        //connection.close(Soup.WebsocketCloseCode.NORMAL, null)
      }
    }
  }

  _closedConnection() {
    printMessage('--- @ericktucto/pusher:src/pusher.js@_closedConnection ---', 'conexion cerrada')
  }

  _errorConnection(_, err) {
    printError('--- ERROR: @ericktucto/pusher:src/pusher.js@_errorConnection ---', err)
  }

  _messageConnection(_, type, data) {
    if (type !== TEXT) return
    const dataString = byteArrayToString(data)
    try {
      const response = JSON.parse(dataString)
      if (response.event === 'pusher:pong') {
        //connection.send_text(JSON.stringify({ event: "pusher:ping" }));
        return
      } else if (response.event === 'pusher:connection_established') {
        //this._proccessPendingChannels();
        return
      } else if (response.event === 'pusher_internal:subscription_succeeded') {
        return
      }
      if (response.channel in this.channels) {
        this.channels[response.channel].dispatch(response.event, dataToStringOrJson(response.data))
      }
    } catch (e) {
      printError('--- @ericktucto/pusher:src/pusher.js@_messageConnection ---', e)
    }
  }

  _proccessPendingChannels() {
    for (let pendingChannel of this.pendingChannels) {
      this._connectChannel(this.channels[pendingChannel])
    }
    this.pendingChannels = []
  }

  _connectChannel(channel) {
    this.connection.send_text(
      JSON.stringify({
        event: 'pusher:subscribe',
        data: { channel: channel.name }
      })
    )
  }

  subscribe(channel) {
    this.selectedChannel = channel
    channel = new Channel(channel, !this.connection)
    if (!(channel.name in this.channels)) {
      this.channels[channel.name] = channel
    }
    if (channel.pending) {
      this.pendingChannels.push(channel.name)
      return this
    }
    this._connectChannel(channel)
    return this
  }

  listen(event, callback) {
    if (this.selectedChannel) {
      this.channels[this.selectedChannel].add(event, callback)
    }
    return this;
  }
}
