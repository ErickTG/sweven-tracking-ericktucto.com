import { getLibSoup } from "./../common.js";

export const createConnection = (uri, protocol = "ws") =>
  new Promise(async (resolve, reject) => {
    try {
      const Soup = await getLibSoup("2.4");
      if (!Soup) resolve(null);
      const session = new Soup.Session();
      uri = new Soup.URI(uri);
      const message = new Soup.Message({ method: "GET", uri });
      session.websocket_connect_async(
        message,
        "origin",
        [protocol],
        null,
        (_session, res) => {
          try {
            resolve(session.websocket_connect_finish(res));
          } catch (e) {
            reject(e);
          }
        }
      );
    } catch (e) {
      logError(e);
    }
  });

export const printError = (msg, e) => {
  log(msg);
  logError(e);
  log(msg);
};

export const printMessage = (msg, i) => {
  log(msg);
  log(e);
  log(msg);
}
