import Soup from '@imports/Soup'

const CONFIG_DEFAULT = { user_agent: 'Mozilla/SpiderMonkey', timeout: 10 }
/**
 * @param {CONFIG_DEFAULT} config
 */
function createSession({ user_agent, timeout }) {
  const session = new Soup.SessionAsync({ user_agent, timeout })
  Soup.Session.prototype.add_feature.call(session, new Soup.ProxyResolverDefault())
  return session
}
export const get = (url, params = {}, config = CONFIG_DEFAULT) => {
  config = typeof config === 'object' ? { ...CONFIG_DEFAULT, ...config } : CONFIG_DEFAULT
  const parseParams = params =>
    Object.keys(params)
      .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
      .join('&')
  // VERIFICAR LOS DATOS QUE SE DESEA ENVIAR
  params = parseParams(params)
  url = params ? `${url}?${params}` : url

  const request = Soup.Message.new('GET', url)
  if ("headers" in config) {
    const { headers } = config
    Object.keys(headers).forEach(header => request.request_headers.append(header, headers[header]))
  }
  request.request_headers.append('Content-Type', 'application/json')

  return new Promise((resolve, reject) => {
    createSession({
      user_agent: config.user_agent,
      timeout: config.timeout
    }).queue_message(request, (_, message) => {
      if (message.status_code !== Soup.KnownStatusCode.OK) {
        reject(message)
      } else {
        resolve(message)
      }
    })
  })
}

export const post = (url, data = {}, config = CONFIG_DEFAULT) => {
  config = typeof config === 'object' ? { ...CONFIG_DEFAULT, ...config } : CONFIG_DEFAULT
  const request = Soup.Message.new('POST', url)
  if ("headers" in config) {
    const { headers } = config
    Object.keys(headers).forEach(header => request.request_headers.append(header, headers[header]))
  }
  request.request_headers.append('Content-Type', 'application/json')

  // VERIFICAR LOS DATOS QUE SE DESEA ENVIAR
  if (Object.keys(data)) {
    const params = JSON.stringify(data)
    request.request_body.append(params)
  }

  return new Promise((resolve, reject) => {
    createSession({
      user_agent: config.user_agent,
      timeout: config.timeout
    }).queue_message(request, (_, message) => {
      if (message.status_code !== Soup.KnownStatusCode.OK) {
        reject(message)
      } else {
        resolve(message)
      }
    })
  })
}
