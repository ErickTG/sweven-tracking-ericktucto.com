import GLib from '@imports/GLib'

export const getJson = path => {
  const [ok, content] = GLib.file_get_contents(path)
  if (ok) {
    try {
      const str = String.fromCharCode(...content)
      return JSON.parse(str)
    } catch (e) {
      logError(e)
    }
  }
  return {}
}

export const setJson = (path, newConfig) => {
  return GLib.file_set_contents(
    path,
    byteArray.fromString(JSON.stringify(newConfig)) // DATOS EN Uint8Array QUE ES IGUAL A ByteArray
  )
}
