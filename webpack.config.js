const path = require('path')

module.exports = {
  entry: path.join(__dirname, 'src', 'index.js'),
  output: {
    path: path.resolve(__dirname),
    filename: 'dist.js'
  },
  externals: {
    '@imports/byteArray': 'imports.byteArray',
    '@imports/Clutter': 'imports.gi.Clutter',
    '@imports/Gio': 'imports.gi.Gio',
    '@imports/GLib': 'imports.gi.GLib',
    '@imports/GObject': 'imports.gi.GObject',
    '@imports/Gtk': 'imports.gi.Gtk',
    '@imports/me': 'imports.misc.extensionUtils.getCurrentExtension()',
    '@imports/Soup': 'imports.gi.Soup',
    '@imports/St': 'imports.gi.St',
    '@imports/ui': 'imports.ui',
    '@imports/this': 'this'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  }
}
