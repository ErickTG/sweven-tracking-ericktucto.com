import { Time } from "./../../../src/libs/time"

test('toString of nullable Time', () => {
  const time = new Time()
  expect(time.toString()).toBe("00:00:00")
})

test('sum method', () => {
  const time1 = new Time("00:59:59")
  const time2 = new Time("00:00:01")
  expect(time1.sum(time2).toString()).toBe("01:00:00")
})
